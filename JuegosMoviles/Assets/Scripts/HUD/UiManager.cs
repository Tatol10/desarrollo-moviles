﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if  UNITY_EDITOR
public class UiManager : MonoBehaviour
{
    GameObject[] pauseObjects;
    GameObject[] InGameObjects;

    
    public void pauseControl()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            showPaused();
            hideInGame();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            hidePaused();
            showInGame();
        }
    }
    public void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }
    public void showInGame()
    {
        foreach (GameObject g in InGameObjects)
        {
            g.SetActive(true);
        }
    }
    public void hideInGame()
    {
        foreach (GameObject g in InGameObjects)
        {
            g.SetActive(false);
        }
    }
    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        InGameObjects = GameObject.FindGameObjectsWithTag("ShowInGame");
        hidePaused();
    }
    // Update is called once per frame
    void Update()
    {

        //uses the p button to pause and unpause the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                showPaused();
                hideInGame();
            }
            else if (Time.timeScale == 0)
            {
                Debug.Log("high");
                Time.timeScale = 1;
                hidePaused();
                showInGame();
            }
        }
    }
    
}
#elif UNITY_ANDROID
public class UiManager : MonoBehaviour
{
    GameObject[] pauseObjects;
    GameObject[] InGameObjects;
    public bool Pause = false;
    public bool Resume = false;


    public void pauseControl()
    {
        if (Time.timeScale == 0)
        {
            Debug.Log("Paused");
            Time.timeScale = 0;
            showPaused();
            hideInGame();
        }
        else if (Time.timeScale == 1)
        {
            Time.timeScale = 1;
            hidePaused();
            showInGame();
        }
    }
    public void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }
    public void showInGame()
    {
        foreach (GameObject g in InGameObjects)
        {
            g.SetActive(true);
        }
    }
    public void hideInGame()
    {
        foreach (GameObject g in InGameObjects)
        {
            g.SetActive(false);
        }
    }
    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        InGameObjects = GameObject.FindGameObjectsWithTag("ShowInGame");
        hidePaused();
        showInGame();
    }
    // Update is called once per frame
    void Update()
    {

        //uses the p button to pause and unpause the game

        if (Pause==true && Time.timeScale == 1)
        {
            Time.timeScale = 0;
            showPaused();
            hideInGame();
            Debug.Log("pause");
            Resume = false;
            
        }
        if (Resume == true && Time.timeScale == 0)
        {
            Debug.Log("resume");
            Time.timeScale = 1;
            hidePaused();
            showInGame();
            Pause = false;

        }


    }

}
#endif
