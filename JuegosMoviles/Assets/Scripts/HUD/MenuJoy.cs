﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuJoy : MonoBehaviour {

    public FixedButton StartButton;
    public FixedButton ExitButton;
    

    bool Exiting ;
    bool Starting ;
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
		Exiting = ExitButton.Pressed;
        Starting = StartButton.Pressed;

		//Debug.Log (StartButton.Pressed);

        if (Starting == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        if (Exiting == true)
        {
			Application.Quit ();
        }
	}
}
