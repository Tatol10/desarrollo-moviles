﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorOptions : MonoBehaviour
{
    CursorLockMode wantedMode;
    void Start()
    {
        wantedMode = wantedMode = CursorLockMode.Locked;
    }
    void SetCursorState()
    {
        Cursor.lockState = wantedMode;
        Cursor.visible = (CursorLockMode.Locked != wantedMode); //hide Cursor when locking

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && wantedMode == CursorLockMode.Locked)
            Cursor.lockState = wantedMode = CursorLockMode.None;
        if (Input.GetKeyDown(KeyCode.Escape) && wantedMode == CursorLockMode.None)
            wantedMode = CursorLockMode.Locked;

        SetCursorState();

    }

}