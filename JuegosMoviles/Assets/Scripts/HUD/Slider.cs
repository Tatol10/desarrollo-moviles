﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthSlider : MonoBehaviour {

    private Slider health;


    public float healthValue;
    

    void Start()
    {
        health = GameObject.Find("HealthSlider").GetComponent<Slider>();
    }


    void Update()
    {
        health.value = healthValue;
        
    }
}
