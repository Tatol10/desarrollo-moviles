using UnityEngine;
using UnityEngine.EventSystems;

public class FixedButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    [HideInInspector]
    public bool Pressed;
    [HideInInspector]
    public bool ButtonDown = false;
    [HideInInspector]
    private bool LastPressed = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Pressed && !LastPressed)
        {
            ButtonDown = true;
            LastPressed = true;
        }
        else
        {
            ButtonDown = false;
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Pressed = false;
        LastPressed = false;
    }
}
