﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDdetect : MonoBehaviour {

    public GameObject HudPC;
    public GameObject HudAndroid;

    void Awake () {
#if UNITY_EDITOR
        GameObject instance = Instantiate(HudPC, transform.position, transform.rotation) as GameObject;

#elif UNITY_ANDROID

        GameObject instance = Instantiate(HudAndroid, transform.position, transform.rotation) as GameObject;

    var instance : GameObject;

#endif

    }

    // Update is called once per frame
    void Update () {
		
	}
}
