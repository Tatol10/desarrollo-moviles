﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyColission : MonoBehaviour {

    public int PlayerDamage = 25;
    Animator EnemyAnim;
    void Start()
    {
        EnemyAnim = GetComponent<Animator>();
    }
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
    
        if (other.gameObject.tag == "Sword") //Realizas Daño
        {
            
            this.gameObject.GetComponent<EnemyStat>().DamageDealed(PlayerDamage);
            Debug.Log("le has hecho " + PlayerDamage + " de dmg , restante : " + gameObject.GetComponent<EnemyStat>().GetEnemyLife());


        }
    }

    
}
