﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStat : MonoBehaviour {

    
    public int EnemyMaxHealth;
    private int EnemyCurrentHealth ;
    Animator EnemyAnim;

    void Start()
    {

        EnemyCurrentHealth = EnemyMaxHealth;

        EnemyAnim = GetComponent<Animator>();
    }
    void Update()
    {
       StartCoroutine(CoroUpdate());
        
    }
   IEnumerator CoroUpdate()
    {
       
        if (EnemyCurrentHealth == 0)
        {
            EnemyAnim.SetTrigger("Deado");
            yield return new WaitForSeconds(2.5f);
            Destroy(gameObject);
        }
                     
    }
        
    public void SetEnemyLife(int Hp) 
    {
        EnemyMaxHealth = Hp;
    }
    public void DamageDealed(int dmg)
    {
        EnemyCurrentHealth -= dmg;
        EnemyAnim.SetTrigger("DamageRecieved");
    }

    public int GetEnemyLife() 
    {

        return EnemyCurrentHealth;
       
    }
    /* IEnumerator CheckLifeStatus()
      {
          if (isDead == true)
          {
              yield return new WaitForSeconds(2.0f);
              Destroy(gameObject);
          }
      }*/
   
}
