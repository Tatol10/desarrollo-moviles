﻿using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using UnityEngine.UI;
public class EnemyFollow : MonoBehaviour
{
    Animator EnemyAnim;
    [HideInInspector]
    public Rigidbody rb;

    [HideInInspector]
    public BoxCollider bc;

    
    public int MoveSpeed = 4;
    public float Vision;
    public float MinDist;
    public float WaitAtack = 2f;
    bool IsMoving = false;




    void Start()
    {
        EnemyAnim = GetComponent<Animator>();
       //bc.isTrigger = false;
       //bc.enabled = false;
        
    }

    void Update()
    {
        var Player = GameObject.FindGameObjectWithTag("Player").transform;

        transform.LookAt(Player);

        if (IsMoving == true)
        {
            EnemyAnim.SetBool("isMoving", true);
        }
        else
        {
            EnemyAnim.SetBool("isMoving", false);

        }
        Vector3 direction = Player.position - this.transform.position;
        direction.y = 0;
        if (Vector3.Distance(transform.position, Player.position) >= MinDist && Vector3.Distance(transform.position, Player.position) <= Vision)
        {
            transform.position += direction.normalized * MoveSpeed * Time.deltaTime;
            //transform.position += transform.forward * MoveSpeed * Time.deltaTime;
            IsMoving = true;

            if (Vector3.Distance(transform.position, Player.position) <= MinDist )
            {
                
                    EnemyAnim.SetTrigger("EnemyAtack");
                if(WaitAtack <= Time.time)
                {
                    EnemyAnim.SetTrigger("EnemyAtack");

                } 
            }
        }
        else
            IsMoving = false;
        


        
    }
    
    void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, Vision);
	}
    void EnableRagdoll()
    {
        rb.isKinematic = false;
        rb.detectCollisions = true;
    }
    void DisableRagdoll()
    {
        rb.isKinematic = true;
        rb.detectCollisions = false;
    }
}
 
 