﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour // estos son los stats del jugador
{
    Slider healthSlider;
    
    

[SerializeField]
    float MaxLife= 100;
    public Scene GameOver ;
   public  float CurrentHp;
    private static PlayerStats instance = null;
    void Start()
    {

       if (GameObject.FindGameObjectWithTag("healthslider"))
        {
            healthSlider = (Slider)FindObjectOfType(typeof(Slider));
        }
        else
        {
            healthSlider = (Slider)FindObjectOfType(typeof(Slider));
        }
        CurrentHp = MaxLife;
        
    }

    void Update ()
    {
        healthSlider.value = PercentageHP();
        if (CurrentHp <= 0) // Si la vida del jugador es 0 o menos, el jugador muere
        {
            Debug.Log("YU DED");
            SceneManager.LoadScene("MainMenu"); // Por ahora si el jugador muere la escena se reinicia
        }
	}

    public static PlayerStats GetPlayerStats()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<PlayerStats>();
        }
        return instance;
    }

    public void SetPlayerLife(int playerLife) // setea la vida
    {
        MaxLife = playerLife;
    }
    public void Heal(int Healing) // cura
    {
        if (CurrentHp > 0 && CurrentHp < 100)
        {
            CurrentHp = Healing;
            PercentageHP();
        }
    }
    public void DamageReceived(int dmg)
    {
        CurrentHp -= dmg;
        healthSlider.value = PercentageHP();
    }

    public float PercentageHP() // entrega la vida
    {
        return CurrentHp / MaxLife;
    }
    private void OnTriggerEnter(Collider other)
    {
        var PlayerHealth = GetComponent<PlayerStats>();
        if (other.gameObject.tag == "HealthPickup")
        {
            PlayerHealth.Heal(100);
            Debug.Log("You have been blessed");

        }
    }
  }
