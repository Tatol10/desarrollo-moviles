﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerStatsMovil : MonoBehaviour // estos son los stats del jugador
{
    public Slider HealthBar;

    [SerializeField]
    float MaxLife;
    public Scene GameOver ;
    float CurrentHp;
    private static PlayerStats instance = null;
    void Start()
    {
        CurrentHp = MaxLife;
        HealthBar.value=PercentageHP();
    }

    void Update ()
    {
        if (CurrentHp <= 0) // Si la vida del jugador es 0 o menos, el jugador muere
        {
            Debug.Log("YU DED");
            SceneManager.LoadScene("MenuMovil"); // Por ahora si el jugador muere la escena se reinicia
        }
	}

    public static PlayerStats GetPlayerStats()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<PlayerStats>();
        }
        return instance;
    }

    public void SetPlayerLife(int playerLife) // setea la vida
    {
        MaxLife = playerLife;
    }
    public void DamageReceived(int dmg)
    {
        CurrentHp -= dmg;
        HealthBar.value = PercentageHP();
    }

    public float PercentageHP() // entrega la vida
    {
        return CurrentHp / MaxLife;
    }
}
