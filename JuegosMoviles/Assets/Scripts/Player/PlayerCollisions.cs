﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour 
{
    public int MinotaurDamage;
    public int KnightDamage;
	void Update ()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        
        
        if (other.gameObject.tag == "Dmg") 
        {
            this.gameObject.GetComponent<PlayerStats>().DamageReceived(KnightDamage);
            Debug.Log("has recibido " +KnightDamage + " de dmg");
        }
        if (other.gameObject.tag == "Minotaur")
        {
            this.gameObject.GetComponent<PlayerStats>().DamageReceived(MinotaurDamage);
            Debug.Log("has recibido "+ MinotaurDamage+ " de dmg");
        }

    }
    
}
