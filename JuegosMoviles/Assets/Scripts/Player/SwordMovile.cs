﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordMovile : MonoBehaviour {

    Animator anim;

    BoxCollider cc;
    public bool AtackAxis = false;
    public bool DefenseAxis = false;


    // Use this for initialization
    void Start()
    {

        anim = GetComponent<Animator>();

        /*cc = GetComponent<BoxCollider>();
        cc.enabled = false;*/

    }


    // Update is called once per frame
    void Update()
    {
        if (DefenseAxis == true)
        {
            anim.SetBool("Block", true);
        }
        else if (DefenseAxis ==false)
        {
            anim.SetBool("Block", false);
        }
        if (AtackAxis == true)
        {
            anim.SetTrigger("Attack");
            AtackAxis = false;
        }
        
        
    }

   /* public void ActivateCollider(int active)
    {
        if (active == 0)
            cc.enabled = false;
        else
            cc.enabled = true;
    }*/




}