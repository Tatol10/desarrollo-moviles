﻿using UnityEngine;
using System.Collections;

public class Moving : MonoBehaviour
{
    
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float RunSpeed;
    public float DashSpeed;
    private bool running = false;
    public float DashCD = 0.5f;
    private float DashTime = 0;
    private float normalspeed ;
    private float m_FieldOfView = 100;
    public float FOVDash = 100;
    [SerializeField]
    private AudioClip mover;
    bool isWalking;
    //testing
    private Vector3 moveDirection = Vector3.zero;
    void Start()
    {
        normalspeed = speed;

    }
    void Update()
    {
        Camera.main.fieldOfView = m_FieldOfView;
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
           
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            isWalking = true;
            if (speed == 0)
                isWalking = false;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;
            if (Input.GetKey(KeyCode.LeftShift)){
                dash();
            }
            else if(Input.GetKeyUp(KeyCode.LeftShift)){
                m_FieldOfView = 100;

            }
            else{
                running = false;
            }
            if (running)
            {
                speed = RunSpeed;
                GetComponent<AudioSource>().PlayOneShot(mover);
            }
            else
            {
                speed = normalspeed;
            }
        }
        if(isWalking == true)
        {
            
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
    void dash()
    {
       if(Time.time > DashTime)
        {
            moveDirection.x *= DashSpeed;
            moveDirection.z *= DashSpeed;
            DashTime =Time.time + DashCD;
            m_FieldOfView = FOVDash;
        }

    }
#if UNITY_EDITOR_WIN
   

#endif
}
