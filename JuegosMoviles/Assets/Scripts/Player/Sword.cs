﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {

    Animator anim;

    BoxCollider cc;
    [SerializeField]
    private AudioClip AtacarSound;

    // Use this for initialization
    void Start()
    {

        anim = GetComponent<Animator>();

        /*cc = GetComponent<BoxCollider>();
        cc.enabled = false;*/

    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            anim.SetBool("Block", true);
        }
        else
        {
            anim.SetBool("Block", false);
        }
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("Attack");
            GetComponent<AudioSource>().PlayOneShot(AtacarSound);
        }


    }

   /* public void ActivateCollider(int active)
    {
        if (active == 0)
            cc.enabled = false;
        else
            cc.enabled = true;
    }*/




}