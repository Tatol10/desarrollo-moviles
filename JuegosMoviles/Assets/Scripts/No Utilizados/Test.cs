﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.position += Vector3.right * InputManager.Instance.GetHorizontalAxis() * 100.0f * Time.deltaTime;	
	}

#if !PRODUCTION_BUILD
    void OnGUI()
    {
        if (GUILayout.Button("Consola Cheats"))
            this.transform.position = Vector3.zero;
    }
#endif
}
