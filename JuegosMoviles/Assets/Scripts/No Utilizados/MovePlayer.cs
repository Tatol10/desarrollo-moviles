﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour {
    public float horizontalSpeed;
	public float verticalSpeed;
	public float runMultiplier;
	private bool running;
	public int velocidad=10;

	void Start () {
	}
	
	void Update () {

		if (Input.GetKey (KeyCode.LeftShift)) {
			running = true;
		} else {
			running = false;
		}

		var x = Input.GetAxis ("Horizontal") * Time.deltaTime * horizontalSpeed;
		var z = Input.GetAxis ("Vertical") * Time.deltaTime * verticalSpeed;

		if (!running) {
			transform.Translate (x, 0, 0);
			transform.Translate (0, 0, z);
		} else {
			transform.Translate (x*runMultiplier, 0, 0);
			transform.Translate (0, 0, z*runMultiplier);
		}
	}
}