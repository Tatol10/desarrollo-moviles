﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    int health = 100;
	[SerializeField]
    float speed;
    [SerializeField]
    float gravity;
    [SerializeField]
    float jumpForce;

    float vSpeed = 0; 

	CharacterController playerControl;
	Vector3 position = Vector3.zero;
    PlayerPosition positionToShare;
    Vector3 jump;
    Rigidbody rb;
    bool isGround;

	void Start ()
    {
		playerControl = GetComponent<CharacterController>();
        positionToShare = PlayerPosition.GetInstance();
        jump = new Vector3(0.0f, 0.0f, 0.0f);
    }
    void OnColission()
    {
        isGround = true;
    }
	
	void Update ()
    {
		position = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		position = transform.TransformDirection(position); 

        positionToShare.SetPosition(GetComponent<Transform>());


        position *= speed;
        

		if (Input.GetKeyDown (KeyCode.A))
		{
			position.x += speed;
		}
		if (Input.GetKeyDown (KeyCode.W))
		{
			position.z += speed;
		}
		if (Input.GetKeyDown (KeyCode.D))
		{
			position.x -= speed;
		}
		if (Input.GetKeyDown (KeyCode.S))
		{
			position.z -= speed;
		}
        if(Input.GetKeyDown(KeyCode.Space) && isGround){
            rb.AddForce(jump * jumpForce,ForceMode.Impulse);
            isGround = false;
        }
        GravityForPlayer(); 
		playerControl.Move (position * Time.deltaTime);
        if (health <= 0)
        {
            this.gameObject.SetActive(false); 
        }
        
    }

    void GravityForPlayer()
    {
        vSpeed -= gravity * Time.deltaTime;
        position.y = vSpeed; 
    }
        
}