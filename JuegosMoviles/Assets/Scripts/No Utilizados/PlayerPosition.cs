﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPosition : MonoBehaviour
{

    private static PlayerPosition instance = null;
    Transform position;
    public static PlayerPosition GetInstance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<PlayerPosition>();
        }
        return instance;
    }
    public void SetPosition(Transform _position)
    {
        position = _position;

    }
    public Transform GetPosition()
    {
        return position;
    }
}
