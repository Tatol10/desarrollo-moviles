﻿
interface IInput
{
    bool GetFireButton();
    float GetHorizontalAxis();
}
