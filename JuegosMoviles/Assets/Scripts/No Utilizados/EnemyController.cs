﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyController : MonoBehaviour {

	public float Vision = 10f;
    public Transform player;

	NavMeshAgent agent;

	void Start () {
        GameObject.FindGameObjectsWithTag("Player");
		agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance (player.position, transform.position);
		if (distance <= Vision) {
			agent.SetDestination (player.position);
			if (distance <= agent.stoppingDistance) {
				//Attack
				FacePlayer();
			}
		}
	}
	void FacePlayer(){
		Vector3 direction = (player.position = transform.position).normalized;
		Quaternion enemyRotation = Quaternion.LookRotation (new Vector3 (direction.x, 1, direction.z));
		transform.rotation = Quaternion.Slerp (transform.rotation, enemyRotation, Time.deltaTime * 5f); 
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, Vision);
	}
}
