Ignacio Leiva
Perez Loguzzo Alejo

Comentario al profe:
1-  Mil perdones que no pueda pasar la version de Android , tube que formatear de mi pc la semana pasada y me olvide de volver a descargar lo necesario para realizar el build , pero funciona.(Alejo Perez Loguzzo)
2-  Tubimos un problema de ultimo movento al intentar cargar los prefabs y no pude relacionar los controles del script JoyConection a los botones en el Hud.


Controles:
 -Pc-
Movimiento : WASD
Saltar : SpaceBar (Considerando Eliminar Mecanica por irrelevancia)
Sprint : shift ( a ser reemplazado por un dash en el futuro)
Pausa : ESC.

Updates:

-Mejoras en el rendimiento , en especial en Android
-Agregado Occlusion Culling
-AutoDetectado de plataforma y carga de Prefabs del menu correspondientes
-Mejora en el menu , ahora se utiliza cliqueando.
-Hud Correctamente anclado en ambas versiones

BugFixes:
-Eliminado bug que hacia que los enemigos atreavesaran el mapa deslizandoce 
-Eliminado bug que muteaba al juego.
-Los enemigos ya no pierden el agro de forma aleatoria
-El arma ya no atravieza al jugador al defender
-No more Ghostly Stairs


A Futuro:
-Implementar dash multisentido
-Agregar armas en la forma de cartas que se eligiran al inicio del nivel , ( prototipo en testeo)
-Nuevo nivel/Enemigos
-Power Move